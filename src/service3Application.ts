import {BootMixin} from '@loopback/boot';
import {ApplicationConfig} from '@loopback/core';
import {RepositoryMixin} from '@loopback/repository';
import {RestApplication} from '@loopback/rest';
import {ServiceMixin} from '@loopback/service-proxy';
import {CardsRepository} from './repositories';
import {PsqlDataSource} from './datasources';
import {Service3Controller} from './controllers';
import {
  RestExplorerBindings,
  RestExplorerComponent,
} from '@loopback/rest-explorer';

export class Service3Application extends BootMixin(
  ServiceMixin(RepositoryMixin(RestApplication)),
) {
  constructor(options: ApplicationConfig = {}) {
    super(options);
    this.bind(RestExplorerBindings.CONFIG).to({
      path: '/explorer',
    });
    this.component(RestExplorerComponent);
    this.projectRoot = __dirname;
    // Customize @loopback/boot Booter Conventions here
    this.bootOptions = {
      controllers: {
        // Customize ControllerBooter Conventions here
        dirs: ['controllers/service3'],
        extensions: ['.controller.js'],
        nested: true,
      },
    };
    this.controller(Service3Controller);
    this.repository(CardsRepository);
    this.dataSource(PsqlDataSource);
  }
}
