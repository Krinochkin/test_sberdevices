require('dotenv').config();
const path = require('path');

export default {
  psql: {
    url:
      process.env.DB_URL ??
      'postgresql://postgres:postgres@sberdevices-postgres:5432/postgres',
    min: +process.env.DB_MIN_CLIENT! || 5,
    max: +process.env.DB_MAX_CLIENT! || 20,
  },
  minio: {
    connection: process.env.MINIO_CONNECTION,
    endPoint: process.env.MINIO_END_POINT ?? 'sberdevices-minio',
    port: +process.env.MINIO_PORT! || 9000,
    accessKey: process.env.MINIO_ACCESS_KEY ?? 'minioadmin',
    secretKey: process.env.MINIO_SECRET_KEY ?? 'minioadmin',
    bucket: process.env.MINIO_STORAGE_BUCKET ?? 'storage',
    useSSL: +process.env.MINIO_USE_SSL! || 0,
  },
  kafka: {
    host: process.env.KAFKA_HOST ?? 'sberdevices-kafka:9092',
    topics: {
      input: process.env.KAFKA_INPUT_TOPIC ?? 'kafka_input',
      output: process.env.KAFKA_OUTPUT_TOPIC ?? 'kafka_output',
      result: process.env.KAFKA_RESULT_TOPIC ?? 'kafka_result',
    },
  },
  storage: {
    root: process.env.STORAGE_PATH ?? path.join(__dirname, '../assets'),
  },
};
