import {KafkaClient, HighLevelProducer, Consumer} from 'kafka-node';
import externalConfig from '../external.config';
import {v4 as uuidv4} from 'uuid';
import map from 'lodash/map';

export default class Kafka {
  protected static instance: Kafka | undefined;

  protected client: KafkaClient;

  protected producer: HighLevelProducer & {ready?: boolean};

  protected consumers: {[id: string]: Consumer} = {};

  protected constructor() {}

  public static getInstance(): Kafka {
    if (!Kafka.instance) {
      Kafka.instance = new Kafka();
    }
    return Kafka.instance;
  }

  public getClient() {
    /*if (!this.client) {
      // @ts-ignore https://github.com/SOHU-Co/kafka-node/issues/846#issuecomment-356456500
      this.client = new KafkaClient({kafkaHost: externalConfig.kafka.host, requestTimeout: false});
    }
    return this.client;*/

    return new KafkaClient({kafkaHost: externalConfig.kafka.host});
  }

  public async getProducer(): Promise<HighLevelProducer> {
    /*if (!this.producer) {
      const client = await this.getClient();
      this.producer = new HighLevelProducer(client, {});
      await new Promise((resolve, reject) => {
        this.producer.on('ready', () => resolve());
        this.producer.on('error', err => reject(err));
      });
    }
    return this.producer;*/

    const client = this.getClient();
    const producer = new HighLevelProducer(client, {});
    return new Promise((resolve, reject) => {
      producer.on('ready', () => resolve(producer));
      producer.on('error', err => reject(err));
    });
  }

  public async sendMessage(topic: string, message: string | object) {
    const producer = await this.getProducer();
    return new Promise((rs, rj) => {
      producer.send(
        [
          {
            topic,
            messages: [JSON.stringify(message)],
          },
        ],
        error => {
          if (error) {
            return rj(error);
          }
          return rs();
        },
      );
    });
  }

  public async getConsumer(topic: string, consumerId = uuidv4()) {
    if (!this.consumers[consumerId]) {
      const client = this.getClient();
      this.consumers[consumerId] = new Consumer(client, [{topic}], {});
    }
    return this.consumers[consumerId];
  }

  async processTopicMessages(
    topic: string,
    processFunction: (content: string) => Promise<void>,
    consumerId = uuidv4(),
  ) {
    const consumer = await this.getConsumer(topic, consumerId);
    consumer.on('message', message => {
      processFunction(String(message.value)).catch(console.error);
    });
    consumer.on('error', err => {
      console.error(err);
      delete this.consumers[consumerId];
      this.processTopicMessages(topic, processFunction).catch(console.error);
    });
  }

  closeConsumer(consumerId: string) {
    if (this.consumers[consumerId]) {
      this.consumers[consumerId].close(console.log);
      delete this.consumers[consumerId];
    }
  }

  closeConnections() {
    map(this.consumers, (el, idx) => this.closeConsumer(idx));
    this.producer.close(console.log);
    this.client.close(console.log);
  }
}
