import {AnyObject} from '@loopback/repository';
import * as _ from 'lodash';

const minio = require('minio');
import externalConfig from '../external.config';
import * as path from 'path';
import * as fs from 'fs';
import {promisify} from 'util';
import {HttpErrors} from '@loopback/rest/dist';

const readFileAsync = promisify(fs.readFile);

/**
 * Класс работы с Minio
 */
export default class Minio {
  protected static instance: Minio | undefined;

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  protected client: any;

  protected constructor() {}

  public static getInstance(): Minio {
    if (!Minio.instance) {
      Minio.instance = new Minio();
    }
    return Minio.instance;
  }

  getConfig() {
    const conf = externalConfig.minio;
    let paredConfig: AnyObject = {};
    if (conf.connection) {
      try {
        paredConfig = JSON.parse(conf.connection);
      } catch (e) {
        console.error('parse minio config', conf.connection, e);
      }
    }
    if (!_.isEmpty(paredConfig)) {
      const searchPort = /:\d*$/;
      // const port = _.get(searchPort.exec(paredConfig.addr), '[0]', 9000);
      // addr;
      return {
        endPoint: paredConfig.addr.replace(searchPort, ''),
        port: 9000, //port.replace(':', ''),
        accessKey: paredConfig.accesskey,
        secretKey: paredConfig.secretkey,
        useSSL: paredConfig.usessl === 'true',
      };
    }
    return {
      endPoint: externalConfig.minio.endPoint,
      port: externalConfig.minio.port,
      useSSL: !!externalConfig.minio.useSSL,
      accessKey: externalConfig.minio.accessKey,
      secretKey: externalConfig.minio.secretKey,
    };
  }

  /**
   * Получение клиента
   * @returns {Promise<*>}
   */
  async getClient() {
    if (this.client) {
      return this.client;
    }
    const config = this.getConfig();
    const client = new minio.Client(config);
    if (!(await this.bucketExists(externalConfig.minio.bucket, client))) {
      await this.makeBucket(externalConfig.minio.bucket, client);
    }
    return client;
  }

  /**
   * Помещение файла в minio
   * @param fileName
   * @param fullPath
   * @param deleteAfterPut
   */
  async fPutObject(
    fileName: string,
    fullPath: string,
    deleteAfterPut?: boolean,
  ): Promise<string> {
    const client = await this.getClient();
    return new Promise((rs, rj) => {
      client.fPutObject(
        externalConfig.minio.bucket,
        fileName,
        fullPath,
        {},
        (err: Error, etag: string) => {
          if (err) {
            return rj(err);
          }
          if (deleteAfterPut) {
            fs.unlink(fullPath, error => {
              if (error) {
                console.log(
                  `Невозможно удалить ${fileName}` + JSON.stringify(error),
                );
              }
            });
          }
          return rs(fileName);
        },
      );
    });
  }

  /**
   * Запись файла из minio на диск
   * @param fileName
   * @param filePath
   */
  async fGetObject(fileName: string, filePath: string): Promise<AnyObject> {
    const client = await this.getClient();
    return new Promise((rs, rj) => {
      client.fGetObject(
        externalConfig.minio.bucket,
        fileName,
        filePath,
        (err: Error & {code: string}) => {
          if (err) {
            if (err.code === 'NotFound') {
              return rj(new HttpErrors.NotFound('File not found!'));
            }
            return rj(Error);
          }
          return rs();
        },
      );
    });
  }

  /**
   * Проверка существования bucket
   * @param bucketName
   * @param minioClient
   */
  async bucketExists(
    bucketName: string,
    minioClient?: typeof minio.Client,
  ): Promise<boolean> {
    const client = minioClient || (await this.getClient());
    return new Promise((rs, rj) => {
      client.bucketExists(bucketName, (err: Error, exists: boolean) => {
        if (err) {
          return rj(err);
        }
        return rs(exists);
      });
    });
  }

  /**
   * Создание bucket
   * @param bucketName
   * @param minioClient
   */
  async makeBucket(
    bucketName: string,
    minioClient?: typeof minio.Client,
  ): Promise<void> {
    const client = minioClient || (await this.getClient());
    return new Promise((rs, rj) => {
      client.makeBucket(bucketName, '', (err: Error) => {
        if (err) {
          return rj(err);
        }
        return rs();
      });
    });
  }

  /**
   * Метод получения файла для REST
   * @param fileName
   * @param res
   */
  async download(
    fileName: string,
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    res: any,
  ): Promise<AnyObject> {
    const fullPath = path.resolve(externalConfig.storage.root, fileName);
    await this.fGetObject(fileName, fullPath);
    const data = await readFileAsync(fullPath);
    fs.unlink(fullPath, err => {
      if (err) {
        console.log(`Невозможно удалить ${fileName}` + JSON.stringify(err));
      }
    });
    return res
      .status(200)
      .attachment(fileName)
      .send(data);
  }
}
