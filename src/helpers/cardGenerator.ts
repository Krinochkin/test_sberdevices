import {Canvas, createCanvas} from 'canvas';
import fs from 'fs';
import path from 'path';
import externalConfig from '../external.config';
import Minio from './minio';

const WIDTH = 500;
const HEIGHT = 350;
const FONT = '30px Calibri';

export class CardGenerator {
  private minio = Minio.getInstance();
  private cardNumber?: number;
  private expirationDate?: string;
  private cardHolder?: string;

  constructor(
    cardNumber?: number,
    expirationDate?: string,
    cardHolder?: string,
  ) {
    this.cardNumber = cardNumber;
    this.expirationDate = expirationDate;
    this.cardHolder = cardHolder;
  }

  setCardNumber(cardNumber: number) {
    this.cardNumber = cardNumber;
  }

  setExpirationDate(expirationDate: string) {
    this.expirationDate = expirationDate;
  }

  setCardHolder(cardHolder: string) {
    this.cardHolder = cardHolder;
  }

  checkParams() {
    if (!this.cardNumber || !this.expirationDate || !this.cardHolder) {
      throw new Error(
        `Не задан обязательный параметр для формирования изображения карты!`,
      );
    }
  }

  fillCanvasContext(canvas: Canvas) {
    const ctx = canvas.getContext('2d');
    const x = WIDTH / 2;
    const y = HEIGHT / 2;
    ctx.fillStyle = '#40bd30';
    ctx.fillRect(0, 0, WIDTH, HEIGHT);
    ctx.font = FONT;
    ctx.textAlign = 'center';
    ctx.fillStyle = '#000000';
    ctx.fillText(
      String(this.cardNumber)
        .replace(/[\d]{4}/g, '$& ')
        .trim(),
      x,
      y,
    );
    ctx.fillText(this.expirationDate!, x, y + 75);
    ctx.textAlign = 'right';
    ctx.fillText(this.cardHolder!, x, y + 125);
  }

  generateFileName(id: number): {fileName: string; fullPath: string} {
    const fileName = `result_${id}_${Math.random()}.png`;
    const fullPath = path.resolve(externalConfig.storage.root, fileName);
    return {fileName, fullPath};
  }

  async generateCard(id: number) {
    this.checkParams();
    const {fileName, fullPath} = this.generateFileName(id);
    const canvas = createCanvas(WIDTH, HEIGHT);
    this.fillCanvasContext(canvas);
    const stream = canvas.createPNGStream();
    const out = fs.createWriteStream(fullPath);
    stream.pipe(out);
    await new Promise((rs, rj) => {
      stream.on('error', rj);
      out.on('error', rj);
      out.on('finish', rs);
    });
    return this.minio.fPutObject(fileName, fullPath, true);
  }
}
