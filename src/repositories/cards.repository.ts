import {DefaultCrudRepository} from '@loopback/repository';
import {Cards, CardsRelations} from '../models';
import {PsqlDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class CardsRepository extends DefaultCrudRepository<
  Cards,
  typeof Cards.prototype.id,
  CardsRelations
> {
  constructor(@inject('datasources.psql') dataSource: PsqlDataSource) {
    super(Cards, dataSource);
  }

  /**
   * Получение следующего id
   */
  async getNextId(): Promise<number> {
    const sqlQuery = `
      SELECT nextval('cards_id_seq');
    `;
    return new Promise((rs, rj) => {
      // eslint-disable-next-line @typescript-eslint/no-floating-promises
      this.dataSource.connector!.execute!(
        sqlQuery,
        undefined,
        undefined,
        (err: Error, result: {nextval: string}[]) => {
          if (err !== null) return rj(err);
          rs(+result[0].nextval || 1);
        },
      );
    });
  }
}
