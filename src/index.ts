import {ApplicationConfig} from '@loopback/core';
import {Service1Application} from './service1Application';
import get = require('lodash/get');
import {Service2Application} from './service2Application';
import {Service3Application} from './service3Application';

const getService = (options: ApplicationConfig) => {
  switch (process.env.SERVICE) {
    case '2':
      return new Service2Application({
        ...options,
        rest: {
          port: get(process, 'env.REST_PORT_SERVICE_2', 3002),
          host: get(process, 'env.REST_HOST_SERVICE_2', '0.0.0.0'),
        },
      });
    case '3':
      return new Service3Application({
        ...options,
        rest: {
          port: get(process, 'env.REST_PORT_SERVICE_3', 3003),
          host: get(process, 'env.REST_HOST_SERVICE_3', '0.0.0.0'),
        },
      });
    default:
      return new Service1Application({
        ...options,
        rest: {
          port: get(process, 'env.REST_PORT_SERVICE_1', 3001),
          host: get(process, 'env.REST_HOST_SERVICE_1', '0.0.0.0'),
        },
      });
  }
};

export async function main(options: ApplicationConfig = {}) {
  const serviceApplication = getService(options);
  await serviceApplication.boot();
  await serviceApplication.migrateSchema();
  await serviceApplication.start();

  const url = serviceApplication.restServer.url;
  console.log(`Service_${process?.env?.SERVICE ?? '1'} is running at ${url}`);

  return serviceApplication;
}
