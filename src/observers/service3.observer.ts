import {
  Getter,
  lifeCycleObserver,
  LifeCycleObserver, // The interface
} from '@loopback/core';
import externalConfig from '../external.config';
import {repository} from '@loopback/repository';
import Kafka from '../helpers/kafka';
import {CardsRepository} from '../repositories';

/**
 * This class will be bound to the application as a `LifeCycleObserver` during
 * `boot`
 */
@lifeCycleObserver('service3')
export class Service3Observer implements LifeCycleObserver {
  private kafka = Kafka.getInstance();

  constructor(
    @repository.getter('CardsRepository')
    protected cardsRepositoryGetter: Getter<CardsRepository>,
  ) {}

  /**
   * This method will be invoked when the application starts
   */
  async start(): Promise<void> {
    const cardsRepository = await this.cardsRepositoryGetter();
    if (process?.env?.SERVICE === '3') {
      await this.kafka.processTopicMessages(
        externalConfig.kafka.topics.output,
        async content => {
          const parsed = JSON.parse(content);
          const result = await cardsRepository.create(
            parsed[Object.keys(parsed)[0]],
          );
          await this.kafka.sendMessage(
            externalConfig.kafka.topics.result,
            result,
          );
        },
      );
    }
  }

  /**
   * This method will be invoked when the application stops
   */
  async stop(): Promise<void> {
    // Add your logic for stop
    this.kafka.closeConnections();
  }
}
