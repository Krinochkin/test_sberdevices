import {
  lifeCycleObserver,
  LifeCycleObserver, // The interface
} from '@loopback/core';
import externalConfig from '../external.config';
import Kafka from '../helpers/kafka';
import {CardGenerator} from '../helpers';

/**
 * This class will be bound to the application as a `LifeCycleObserver` during
 * `boot`
 */
@lifeCycleObserver('service2')
export class Service2Observer implements LifeCycleObserver {
  private kafka = Kafka.getInstance();

  constructor() {}

  /**
   * This method will be invoked when the application starts
   */
  async start(): Promise<void> {
    if (process?.env?.SERVICE === '2') {
      await this.kafka.processTopicMessages(
        externalConfig.kafka.topics.input,
        async content => {
          const parsed = JSON.parse(content);
          const cardGenerator = new CardGenerator(
            parsed.cardNumber,
            parsed.expirationDate,
            parsed.cardHolder,
          );
          const img = await cardGenerator.generateCard(parsed.id);
          await this.kafka.sendMessage(externalConfig.kafka.topics.output, {
            [parsed.id]: {...parsed, img},
          });
        },
      );
    }
  }

  /**
   * This method will be invoked when the application stops
   */
  async stop(): Promise<void> {
    // Add your logic for stop
    this.kafka.closeConnections();
  }
}
