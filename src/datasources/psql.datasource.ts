import {inject} from '@loopback/core';
import {juggler} from '@loopback/repository';
import config from './psql.datasource.config.json';
import externalConfig from '../external.config';

export class PsqlDataSource extends juggler.DataSource {
  static dataSourceName = 'psql';

  constructor(
    @inject('datasources.config.psql', {optional: true})
    dsConfig: object = {...config, ...externalConfig.psql},
  ) {
    super(dsConfig);
  }
}
