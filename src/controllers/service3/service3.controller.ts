import {AnyObject, repository} from '@loopback/repository';
import {param, get, RestBindings, Response} from '@loopback/rest';
import {CardsRepository} from '../../repositories';
import Kafka from '../../helpers/kafka';
import Minio from '../../helpers/minio';
import {inject} from '@loopback/core';

export class Service3Controller {
  private kafka = Kafka.getInstance();
  private minio = Minio.getInstance();

  constructor(
    @inject(RestBindings.Http.RESPONSE) public response: Response,
    @repository(CardsRepository)
    public cardsRepository: CardsRepository,
  ) {}

  @get('/api/card/img', {
    responses: {
      '200': {
        description: 'Download a File within specified Container',
        content: {'application/json': {schema: {'x-ts-type': Object}}},
      },
    },
  })
  async getFileByName(
    @param.query.string('id') id: number,
  ): Promise<AnyObject> {
    const record = await this.cardsRepository.findById(id);
    return this.minio.download(record.img, this.response);
  }
}
