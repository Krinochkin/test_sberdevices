import {repository} from '@loopback/repository';
import {post, getModelSchemaRef, requestBody} from '@loopback/rest';
import {Cards} from '../../models';
import {CardsRepository} from '../../repositories';
import externalConfig from '../../external.config';
import Kafka from '../../helpers/kafka';

export class Service1Controller {
  private kafka = Kafka.getInstance();
  constructor(
    @repository(CardsRepository)
    public cardsRepository: CardsRepository,
  ) {}

  @post('/api/card', {
    responses: {
      '200': {
        description: 'Cards model instance',
        content: {'application/json': {schema: getModelSchemaRef(Cards)}},
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Cards, {
            title: 'NewCards',
            exclude: ['id', 'img'],
          }),
        },
      },
    })
    cards: Omit<Cards, 'id'>,
  ): Promise<Cards> {
    const id = await this.cardsRepository.getNextId();
    await this.kafka.sendMessage(externalConfig.kafka.topics.input, {
      ...cards,
      id,
    });
    return new Promise(rs => {
      this.kafka
        .processTopicMessages(
          externalConfig.kafka.topics.result,
          async content => {
            const parsed = JSON.parse(content);
            if (parsed.id === id) {
              this.kafka.closeConsumer(String(id));
              return rs(parsed);
            }
          },
          String(id),
        )
        .catch(console.error);
    });
  }
}
