import {Entity, model, property} from '@loopback/repository';

@model({
  settings: {
    forceId: false,
  },
})
export class Cards extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
    postgresql: {
      columnName: 'id',
    },
  })
  id: number;

  @property({
    required: true,
    type: 'string',
    postgresql: {
      dataType: 'bigint',
      columnName: 'cardNumber',
    },
  })
  cardNumber: number;

  @property({
    required: true,
    type: 'string',
    jsonSchema: {
      pattern: '^(0[1-9]|1[0-2])\\/[0-9]{2}$',
    },
    postgresql: {
      dataType: 'varchar(5)',
      columnName: 'expirationDate',
    },
  })
  expirationDate: string;

  @property({
    type: 'string',
    required: true,
    jsonSchema: {
      minimum: 0,
      maximum: 999,
    },
    postgresql: {
      dataType: 'smallint',
      columnName: 'cv',
    },
  })
  cv: number;

  @property({
    type: 'string',
    required: true,
    postgresql: {
      dataType: 'varchar',
      columnName: 'cardHolder',
    },
  })
  cardHolder: string;

  @property({
    type: 'string',
    required: true,
    postgresql: {
      dataType: 'varchar',
      columnName: 'img',
    },
  })
  img: string;

  constructor(data?: Partial<Cards>) {
    super(data);
  }
}

export interface CardsRelations {
  // describe navigational properties here
}

export type CardsWithRelations = Cards & CardsRelations;
