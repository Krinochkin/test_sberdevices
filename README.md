# test-sberdevices
Приложение реализовано на Loopback 4.  
Данный фреймворк на базе Express выбран из-за удобного взаимодействия с любыми БД и реализующий технологию ORM из коробки.  
Вместо реализации 3-х сервисов, я выбрал подход Monolith first, т.к. он более подходит для быстрого начального запуска приложения, с возможным последующим легким разбиением на отдельные сервисы.

## Запуск приложения через docker-compose
1. Перейти в каталог проекта
2. Выполнить команду
`docker-compose up`  
   
## Запуск приложение без docker
1. Установить завимости `npm ci`  
2. Скопировать файл `.env.example` с новым именем `.env`
3. Заполнить завимости `DB_URL`, `MINIO_END_POINT`, `KAFKA_HOST` и другие при необходимости для настройки подключения к postgresql, minio и kafka
4. Создать топики `kafka_input`, `kafka_output` и `kafka_result`
5. Выполнить `npm run build` для сборки
6. Выполнить `npm run migrate` при первом запуске для миграции БД
7. Выполнить `npm run start_1`, `npm run start_2` и `npm run start_3` для запуска сервисов 1, 2 и 3

GUI Api для Service_1: http://localhost:3001/explorer/  
GUI Api для Service_3: http://localhost:3003/explorer/

[![LoopBack](https://github.com/strongloop/loopback-next/raw/master/docs/site/imgs/branding/Powered-by-LoopBack-Badge-(blue)-@2x.png)](http://loopback.io/)
